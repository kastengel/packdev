###  **Packdev**

Packdev is a laravel package to help you develop a laravel specific package. It will automatically set the paths needed and simulate it like when you are on laravel environment.

#### **Note**

|Laravel version|Package version  |
|--|--|
| 5.5.* | 1.0.*
| 5.6.* | 1.1.*
| 5.7.* | 1.2.*
| 5.8.* | 1.3.*
| ^6.0 | 1.4.*

#### **Instalation**

    composer require kastengel/packdev --dev

#### **Service providers and Aliases**

To register service providers and aliases, do it in composer.json and this package will be simulate your package to be discovered by laravel package discovery.

#### **Paths**
This package reregister laravel path. Below is the structure of the path

 1. "bootstrap" directory is located at packdev directory "vendor/kastengel/pacdev/bootstrap"
 2. "public" directory is located at packdev directory "vendor/kastengel/pacdev/public"
 3. "resources" directory is located at laravel skeleton directory "vendor/laravel/laravel/resources"
 4. "storage" directory is located at laravel skeleton directory "vendor/laravel/laravel/storage"
 5. "database" directory is located at laravel skeleton directory "vendor/laravel/laravel/database"
 6. "config" directory is located at laravel skeleton directory "vendor/laravel/laravel/config"
 7. "base path" is located at your package root dir

**Your package factories will automatically loaded during test run.**

#### **Artisan**

 To use artisan, use this package artisan at "vendor/kastengel/packdev/artisan". Because of paths modification, this package provide some commands in order to apply your package context. Below are the custom commands

  1. dusk:pacdev-install : Install laravel dusk scaffolding
  2. dusk:pacdev-make : Create new dusk test
  3. dusk:pacdev-page : Create new dusk page
  4. make:packdev-factory : Create new factory
  5. make:packdev-migration : Create new migration
  6. make:packdev-model : Create new model
  7. make:packdev-seeder: Create new seeder
  8. make:packdev-test : Create new test
  9. vendor:link : Package assets linking by creating symlink instead of copying

#### **Tests**
To run test, run this following command

    phpunit
    php artisan dusk
