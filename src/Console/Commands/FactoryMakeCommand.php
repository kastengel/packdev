<?php

namespace Kastengel\Packdev\Console\Commands;

use Illuminate\Database\Console\Factories\FactoryMakeCommand as LaravelBase;

class FactoryMakeCommand extends LaravelBase
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:packdev-factory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model factory';

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = str_replace(
            ['\\', '/'], '', $this->argument('name')
        );

        return $this->laravel->basePath()."/database/factories/{$name}.php";
    }
}
