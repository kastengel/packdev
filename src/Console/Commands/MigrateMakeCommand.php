<?php

namespace Kastengel\Packdev\Console\Commands;

use  Illuminate\Database\Console\Migrations\MigrateMakeCommand as LaravelBase;

class MigrateMakeCommand extends LaravelBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
     protected $signature = 'make:packdev-migration {name : The name of the migration.}
         {--create= : The table to be created.}
         {--table= : The table to migrate.}
         {--path= : The location where the migration file should be created.}
         {--realpath : Indicate any provided migration file paths are pre-resolved absolute paths}
         {--fullpath : Output the full path of the migration}';

     /**
      * The console command description.
      *
      * @var string
      */
     protected $description = 'Create a new migration file';

     /**
      * Get the path to the migration directory.
      *
      * @return string
      */
     protected function getMigrationPath()
     {
         if (! is_null($targetPath = $this->input->getOption('path'))) {
             return $this->laravel->basePath().'/'.$targetPath;
         }

         return $this->laravel->basePath().DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR.'migrations';
     }
}
