<?php

namespace Kastengel\Packdev\Console\Commands;

use Illuminate\Foundation\Console\TestMakeCommand as LaravelBase;

class TestMakeCommand extends LaravelBase
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:packdev-test {name : The name of the class} {--unit : Create a unit test}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new test class';

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return $this->laravel->getNamespace().'Tests';
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            ['DummyNamespace', 'use Tests'],
            [$this->getNamespace($name), 'use Kastengel\Packdev\Tests'],
            $stub
        );

        return $this;
    }
}
