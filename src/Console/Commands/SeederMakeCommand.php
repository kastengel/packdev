<?php

namespace Kastengel\Packdev\Console\Commands;

use Illuminate\Database\Console\Seeds\SeederMakeCommand as LaravelBase;

class SeederMakeCommand extends LaravelBase
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:packdev-seeder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new seeder class';

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        return $this->laravel->basePath().'/database/seeds/'.$name.'.php';
    }
}
