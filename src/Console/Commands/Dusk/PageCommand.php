<?php

namespace Kastengel\Packdev\Console\Commands\Dusk;

use Laravel\Dusk\Console\PageCommand as LaravelBase;

class PageCommand extends LaravelBase
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'dusk:packdev-page {name : The name of the class}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Dusk page class';

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return $this->laravel->getNamespace().'Tests';
    }
}
