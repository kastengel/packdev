<?php

namespace Kastengel\Packdev\Console\Commands\Dusk;

use Laravel\Dusk\Console\MakeCommand as LaravelBase;

class MakeCommand extends LaravelBase
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'dusk:packdev-make {name : The name of the class}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Dusk test class';

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return $this->laravel->getNamespace().'Tests';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/test.stub';
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            ['DummyNamespace', 'DummyBaseClass'],
            [$this->getNamespace($name), $this->rootNamespace().'\DuskTestCase'],
            $stub
        );

        return $this;
    }
}
