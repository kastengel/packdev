<?php

namespace Kastengel\Packdev\Console\Commands\Dusk;

use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use Laravel\Dusk\Console\InstallCommand as LaravelBase;

class InstallCommand extends LaravelBase
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dusk:packdev-install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Dusk into the application';

    /**
     * Create new instance
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! is_dir(base_path('tests/Browser/Pages'))) {
            mkdir(base_path('tests/Browser/Pages'), 0755, true);
        }

        if (! is_dir(base_path('tests/Browser/screenshots'))) {
            $this->createScreenshotsDirectory();
        }

        if (! is_dir(base_path('tests/Browser/console'))) {
            $this->createConsoleDirectory();
        }

        $stubs = [
            [
                'name' => $this->qualifyClass('DuskTestCase'),
                'stub' => __DIR__.'/stubs/DuskTestCase.stub',
                'destination' => base_path('tests/DuskTestCase.php')
            ],
            [
                'name' => $this->qualifyClass('Browser/ExampleTest'),
                'stub' => __DIR__.'/stubs/ExampleTest.stub',
                'destination' => base_path('tests/Browser/ExampleTest.php')
            ],
            [
                'name' => $this->qualifyClass('Browser/Pages/Page'),
                'stub' => __DIR__.'/stubs/Page.stub',
                'destination' => base_path('tests/Browser/Pages/Page.php')
            ],
        ];

        foreach ($stubs as $stub) {
            $this->generateFile($stub['name'], $stub['stub'], $stub['destination']);
        }

        $this->info('Dusk scaffolding installed successfully.');
    }

    /**
     * Write files to destination
     *
     * @param  string $name
     * @param  string $stub
     * @param  string $destination 
     *
     * @return void
     */
    protected function generateFile($name, $stub, $destination)
    {
        $this->files->put($destination, $this->buildClass($name, $stub));
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

    /**
     * Parse the class name and format according to the root namespace.
     *
     * @param  string  $name
     * @return string
     */
    protected function qualifyClass($name)
    {
        $rootNamespace = $this->rootNamespace();

        if (Str::startsWith($name, $rootNamespace)) {
            return $name;
        }

        $name = str_replace('/', '\\', $name);

        return $this->qualifyClass(
            $this->getDefaultNamespace(trim($rootNamespace, '\\')).'\\'.$name
        );
    }

    /**
     * Get the full namespace for a given class, without the class name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getNamespace($name)
    {
        return trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @param  string $stub
     *
     * @return string
     */
    protected function buildClass($name, $stub)
    {
        $stub = $this->files->get($stub);

        return $this->replaceNamespace($stub, $name);
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace($stub, $name)
    {
        $stub = str_replace(
            ['DummyNamespace', 'DummyBaseClass'],
            [$this->getNamespace($name), $this->rootNamespace().'\DuskTestCase'],
            $stub
        );

        return $stub;
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return $this->laravel->getNamespace().'Tests';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }
}
