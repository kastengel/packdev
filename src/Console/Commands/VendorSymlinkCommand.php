<?php

namespace Kastengel\Packdev\Console\Commands;

use Illuminate\Foundation\Console\VendorPublishCommand;

class VendorSymlinkCommand extends VendorPublishCommand
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'vendor:link {--force : Overwrite any existing files.}
                    {--all : Publish assets for all service providers without prompt.}
                    {--provider= : The service provider that has assets you want to publish.}
                    {--tag=* : One or many tags that have assets you want to publish.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Vendor assets linking';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->determineWhatShouldBePublished();

        foreach ($this->tags ?: [null] as $tag) {
            $this->publishTag($tag);
        }

        $this->info('Linking complete.');
    }

    /**
     * Publish the given item from and to the given location.
     *
     * @param  string  $from
     * @param  string  $to
     * @return void
     */
    protected function publishItem($from, $to)
    {
        $this->ensureDirectoryExists($to);

        if(!$this->files->exists($to)) {
            return $this->files->link($from, $to);
        }

        $this->error("Destination <{$to}> already exists");
    }

    /**
     * Ensure directory exists before making symlink
     * 
     * @param   string  $path
     * 
     * @return  void
     */
    protected function ensureDirectoryExists($path)
    {
        $paths = explode(DIRECTORY_SEPARATOR, $path);
        $poppedPath = array_pop($paths);
        $directory = implode(DIRECTORY_SEPARATOR, $paths);

        if(!$this->files->exists($directory)) {
            $this->files->makeDirectory($directory, 0755, true);
        }
    }
}
