<?php

namespace Kastengel\Packdev\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Foundation\Console\ModelMakeCommand as LaravelBase;

class ModelMakeCommand extends LaravelBase
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:packdev-model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Eloquent model class';

    /**
     * Create a model factory for the model.
     *
     * @return void
     */
    protected function createFactory()
    {
        $this->call('make:packdev-factory', [
            'name' => $this->argument('name').'Factory',
            '--model' => $this->argument('name'),
        ]);
    }

    /**
     * Create a migration file for the model.
     *
     * @return void
     */
    protected function createMigration()
    {
        $table = Str::plural(Str::snake(class_basename($this->argument('name'))));

        $this->call('make:packdev-migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);
    }
}
