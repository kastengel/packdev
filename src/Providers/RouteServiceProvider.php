<?php

namespace Kastengel\Packdev\Providers;

use Illuminate\Support\Facades\Route;
use App\Providers\RouteServiceProvider as LaravelBase;

class RouteServiceProvider extends LaravelBase
{
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $vendorPath = $this->app->vendorPath();
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group($vendorPath.'/laravel/laravel/routes/web.php');
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        $vendorPath = $this->app->vendorPath();
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group($vendorPath.'/laravel/laravel/routes/api.php');
    }
}
