<?php

namespace Kastengel\Packdev\Providers;

use Illuminate\Support\ServiceProvider;

class PackdevServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $vendorPath = $this->app->vendorPath();

        $this->commands([
            \Kastengel\Packdev\Console\Commands\Dusk\MakeCommand::class,
            \Kastengel\Packdev\Console\Commands\Dusk\PageCommand::class,
            \Kastengel\Packdev\Console\Commands\Dusk\InstallCommand::class,
            \Kastengel\Packdev\Console\Commands\FactoryMakeCommand::class,
            \Kastengel\Packdev\Console\Commands\MigrateMakeCommand::class,
            \Kastengel\Packdev\Console\Commands\ModelMakeCommand::class,
            \Kastengel\Packdev\Console\Commands\SeederMakeCommand::class,
            \Kastengel\Packdev\Console\Commands\TestMakeCommand::class,
            \Kastengel\Packdev\Console\Commands\VendorSymlinkCommand::class,
        ]);

        $this->app->make(\Illuminate\Database\Eloquent\Factory::class)->load(realpath($vendorPath.'/../database/factories'));
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register(){}
}
