<?php

namespace Kastengel\Packdev\Foundation\Bootstrap;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;

class RegisterProviders
{
    /**
     * Bootstrap the given application.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function bootstrap(Application $app)
    {
        $providers = collect($app->config['app.providers'])->filter(function($value){
            return $value != \App\Providers\RouteServiceProvider::class;
        })
        ->merge($this->getPackageProviders($app->basePath()));

        $app->config->set('app.providers', $providers->toArray());

        $app->registerConfiguredProviders();
    }

    /**
     * Retreive developed package manifest
     *
     * @param  string $path
     *
     * @return array
     */
    protected function getPackageProviders($path)
    {
        $files = new Filesystem;

        $providers = [];

        if ($files->exists($path = $path.'/composer.json')) {
            $providers = collect(json_decode($files->get($path), true))->filter(function($value, $key){
                return $key == 'extra';
            })->flatMap(function($values){
                return $values;
            })->filter(function($value, $key){
                return $key == 'laravel';
            })->flatMap(function($values){
                return $values;
            })->filter(function($value, $key){
                return $key == 'providers';
            })->flatten();
        }

        return $providers;
    }
}
