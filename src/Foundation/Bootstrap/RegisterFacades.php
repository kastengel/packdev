<?php

namespace Kastengel\Packdev\Foundation\Bootstrap;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Facade;
use Illuminate\Foundation\PackageManifest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Filesystem\Filesystem;

class RegisterFacades
{
    /**
     * Bootstrap the given application.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function bootstrap(Application $app)
    {
        Facade::clearResolvedInstances();

        Facade::setFacadeApplication($app);

        $aliases = $this->getPackageAliases($app->basePath());

        AliasLoader::getInstance(array_merge(
            $app->make('config')->get('app.aliases', []),
            $app->make(PackageManifest::class)->aliases(),
            $aliases
        ))->register();
    }

    /**
     * Retreive developed package manifest
     *
     * @param  string $path
     *
     * @return array
     */
    protected function getPackageAliases($path)
    {
        $files = new Filesystem;

        $aliases = [];

        if ($files->exists($path = $path.'/composer.json')) {
            $aliases = collect(json_decode($files->get($path), true))->filter(function($value, $key){
                return $key == 'extra';
            })->flatMap(function($values){
                return $values;
            })->filter(function($value, $key){
                return $key == 'laravel';
            })->flatMap(function($values){
                return $values;
            })->filter(function($value, $key){
                return $key == 'aliases';
            })->flatMap(function($values){
                return $values;
            });
        }

        return $aliases->all();
    }
}
