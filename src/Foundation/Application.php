<?php

namespace Kastengel\Packdev\Foundation;

use ReflectionClass;
use Illuminate\Foundation\Application as LaravelBase;

class Application extends LaravelBase
{
    /**
     * Get the path to the application "app" directory.
     *
     * @param string $path Optionally, a path to append to the app path
     * @return string
     */
    public function path($path = '')
    {
        $appPath = $this->appPath ?: $this->basePath.DIRECTORY_SEPARATOR.'src';

        return $appPath.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Get the path to the bootstrap directory.
     *
     * @param string $path Optionally, a path to append to the bootstrap path
     * @return string
     */
    public function bootstrapPath($path = '')
    {
        $root = $this->getRoot();

        return $root.DIRECTORY_SEPARATOR.'bootstrap'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Get the path to the public / web directory.
     *
     * @return string
     */
    public function publicPath()
    {
        $root = $this->getRoot();

        return $root.DIRECTORY_SEPARATOR.'public';
    }

    /**
     * Get the path to the storage directory.
     *
     * @return string
     */
    public function storagePath()
    {
        $laravel = $this->vendorPath().DIRECTORY_SEPARATOR.'laravel'.DIRECTORY_SEPARATOR.'laravel';

        return $laravel.DIRECTORY_SEPARATOR.'storage';
    }

    /**
     * Get the path to the application configuration files.
     *
     * @param string $path Optionally, a path to append to the config path
     * @return string
     */
    public function configPath($path = '')
    {
        $laravel = $this->vendorPath().DIRECTORY_SEPARATOR.'laravel'.DIRECTORY_SEPARATOR.'laravel';

        return $laravel.DIRECTORY_SEPARATOR.'config'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Get the path to the resources directory.
     *
     * @param  string  $path
     * @return string
     */
    public function resourcePath($path = '')
    {
        $laravel = $this->vendorPath().DIRECTORY_SEPARATOR.'laravel'.DIRECTORY_SEPARATOR.'laravel';

        return $laravel.DIRECTORY_SEPARATOR.'resources'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Get the path to the database directory.
     *
     * @param string $path Optionally, a path to append to the database path
     * @return string
     */
    public function databasePath($path = '')
    {
        $laravel = $this->vendorPath().DIRECTORY_SEPARATOR.'laravel'.DIRECTORY_SEPARATOR.'laravel';

        return ($this->databasePath ?: $laravel.DIRECTORY_SEPARATOR.'database').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Retrieve vendor directory
     *
     * @return string
     */
    public function vendorPath()
    {
        $classLoader = new ReflectionClass(\Illuminate\Foundation\Application::class);
        $classLoaderDir = dirname($classLoader->getFileName());

        return realpath($classLoaderDir.'/../../../../../');
    }

    /**
     * Retrieve root dir
     *
     * @return string
     */
    protected function getRoot()
    {
        $root = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;

        return realpath($root);
    }
}
