<?php

namespace Kastengel\Packdev\Tests\Feature;

use Schema;
use Artisan;
use Kastengel\Packdev\Tests\TestCase;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EnvirontmentTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test if it load right console kernel
     *
     * @return void
     */
    public function testItLoadRightKernel()
    {
        $kernel = $this->app->make(\Illuminate\Contracts\Console\Kernel::class);
        $this->assertSame(get_class($kernel), \Kastengel\Packdev\Console\Kernel::class);
    }

    /**
     * Test if it register this own providers
     *
     * @return void
     */
    public function testItRegisterProviders()
    {
        $this->assertTrue(in_array('Kastengel\\Packdev\\Providers\\PackdevServiceProvider', $this->app->getLoadedProviders()));
    }

    /**
     * Test if it register this own facades
     *
     * @return void
     */
    public function testItRegisterFacades()
    {
        $aliases = AliasLoader::getInstance()->getAliases();
        $this->assertTrue(isset($aliases['Alias']));
    }

    /**
     * Test if it load laravel migrations
     *
     * @return void
     */
    public function testItLoadLaravelMigration()
    {
        $this->assertTrue(Schema::hasTable('users'));
    }

    /**
     * Test if it load laravel translations
     *
     * @return void
     */
    public function testItLoadLaravelLang()
    {
        $this->assertSame(trans('auth.failed'), 'These credentials do not match our records.');
    }
}
