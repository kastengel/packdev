<?php

namespace Kastengel\Packdev\Tests\Feature;

use Kastengel\Packdev\Tests\TestCase;

class HttpTest extends TestCase
{
    /**
     * Test if it load laravel routes
     *
     * @return void
     */
    public function testItLoadLaravelRoute()
    {
        $this->get('/')
             ->assertViewIs('welcome');
    }
}
