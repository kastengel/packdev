<?php

namespace Kastengel\Packdev\Tests\Feature;

use Artisan;
use Kastengel\Packdev\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommandTest extends TestCase
{
    /**
     * Test dusk install command
     *
     * @return void
     */
    public function testDuskInstall()
    {
        @unlink(base_path('tests/DuskTestCase.php'));
        $this->delTree(base_path('tests/Browser'));

        Artisan::call('dusk:packdev-install');

        $duskTestCase = new \ReflectionClass(\Kastengel\Packdev\Tests\DuskTestCase::class);

        $this->assertTrue($duskTestCase->isAbstract());
        $this->assertSame($duskTestCase->getFilename(), base_path('tests/DuskTestCase.php'));

        $this->assertTrue(is_dir(base_path('tests/Browser/Pages')));
        $this->assertTrue(is_dir(base_path('tests/Browser/screenshots')));
        $this->assertTrue(is_dir(base_path('tests/Browser/console')));
    }

    /**
     * Test dusk make command
     *
     * @return void
     */
    public function testDuskMake()
    {
        @unlink(base_path('tests/Browser/DumbTest.php'));

        Artisan::call('dusk:packdev-make', [
            'name' => 'DumbTest'
        ]);

        $test = new \ReflectionClass(\Kastengel\Packdev\Tests\Browser\DumbTest::class);
        $this->assertSame($test->getFilename(), base_path('tests/Browser/DumbTest.php'));
    }

    /**
     * Test dusk page command
     *
     * @return void
     */
    public function testDuskPage()
    {
        @unlink(base_path('tests/Browser/Pages/DumbPage.php'));

        Artisan::call('dusk:packdev-page', [
            'name' => 'DumbPage'
        ]);

        $test = new \ReflectionClass(\Kastengel\Packdev\Tests\Browser\Pages\DumbPage::class);
        $this->assertSame($test->getFilename(), base_path('tests/Browser/Pages/DumbPage.php'));
    }

    /**
     * Test factory make command
     *
     * @return void
     */
    public function testFactoryMake()
    {
        @unlink(base_path('database/factories/DumbFactory.php'));

        Artisan::call('make:packdev-factory', [
            'name' => 'DumbFactory'
        ]);

        $this->assertTrue(file_exists(base_path('database/factories/DumbFactory.php')));
    }

    /**
     * Test migrate make command
     *
     * @return void
     */
    public function testMigrateMake()
    {
        $dir = base_path('database/migrations/dumb/');
        array_map('unlink', glob($dir.'/*'));

        Artisan::call('make:packdev-migration', [
            'name' => 'create_dumb_table',
            '--path' => 'database/migrations/dumb'
        ]);

        $files = glob($dir . '/*');

        $this->assertSame(count($files), 1);
    }

    /**
     * Test model make command
     *
     * @return void
     */
    public function testModelMake()
    {
        @unlink(app_path('DumbModel.php'));

        Artisan::call('make:packdev-model', [
            'name' => 'DumbModel'
        ]);

        $test = new \ReflectionClass(\Kastengel\Packdev\DumbModel::class);
        $this->assertSame($test->getFilename(), app_path('DumbModel.php'));
    }

    /**
     * Test seeder make command
     *
     * @return void
     */
    public function testSeederMake()
    {
        @unlink(base_path('database/seeds/DumbSeeder.php'));

        Artisan::call('make:packdev-seeder', [
            'name' => 'DumbSeeder'
        ]);

        $test = new \ReflectionClass(\DumbSeeder::class);
        $this->assertSame($test->getFilename(), base_path('database/seeds/DumbSeeder.php'));
    }

    /**
     * Test test make command
     *
     * @return void
     */
    public function testTestMake()
    {
        @unlink(base_path('tests/Feature/DumbTest.php'));

        Artisan::call('make:packdev-test', [
            'name' => 'DumbTest'
        ]);

        $test = new \ReflectionClass(\Kastengel\Packdev\Tests\Feature\DumbTest::class);
        $this->assertSame($test->getFilename(), base_path('tests/Feature/DumbTest.php'));
    }

    /**
     * Test vendor symlink
     *
     * @return void
     */
    public function testVendorSymlink()
    {
        @unlink(base_path('vendor/laravel/laravel/resources/views/vendor/mail'));

        Artisan::call('vendor:link', [
            '--tag' => 'laravel-mail'
        ]);

        $this->assertTrue(file_exists(base_path('vendor/laravel/laravel/resources/views/vendor/mail')));
    }

    /**
     * Remove dir recursively
     *
     * @param  string $dir
     *
     * @return bool
     */
    protected function delTree($dir) {
        $files = array_diff(scandir($dir), array('.','..'));

        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }

        return rmdir($dir);
  }
}
