<?php

namespace Kastengel\Packdev\Tests\Feature;

use Kastengel\Packdev\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FactoryTest extends TestCase
{
    /**
     * Test if factory is loaded
     *
     * @return void
     */
    public function testItIsLoaded()
    {
        $tests = factory(\Kastengel\Packdev\Test::class, 3)->make();

        $this->assertSame($tests->count(), 3);
        $this->assertTrue(strlen($tests->first()->description) > 0);
    }
}
