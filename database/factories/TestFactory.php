<?php

use Faker\Generator as Faker;

$factory->define(Kastengel\Packdev\Test::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text
    ];
});
